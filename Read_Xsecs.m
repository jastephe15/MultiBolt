% Script to read LXcat files. Taken from the Monte Carlo code, METHES, 
% with permission. Credit to Mohamed Rabie and Christian Franck for 
% developing portions of this script.
% 
% M. Rabie, C.M. Franck, "METHES: A Monte Carlo collision code for the 
% simulation of electron transport in low temperature plasmas",
% Comp. Phys. Comm. 203, 268-277 (2016).

clear st s

Biagi_Ref1Test_string = ['Cross-Sections/Biagi_Ar.txt',  ...
                         'Cross-Sections/Biagi_H2.txt', ...
                         'Cross-Sections/Biagi_He.txt', ...
                         'Cross-Sections/Biagi_Kr.txt', ...
                         'Cross-Sections/Biagi_N2.txt', ...
                         'Cross-Sections/Biagi_Ne.txt', ...
                         'Cross-Sections/Biagi_O2.txt', ...
                         'Cross-Sections/Biagi_SF6.txt', ...
                         'Cross-Sections/Biagi_Xe.txt'];
                     
Hayashi_Ref1Test_string = ['Cross-Sections/Hayashi_C2H2.txt',  ...
                           'Cross-Sections/Hayashi_C2H4.txt', ...
                           'Cross-Sections/Hayashi_C2H6.txt', ...
                           'Cross-Sections/Hayashi_CCl2F2.txt', ...
                           'Cross-Sections/Hayashi_CCl4.txt', ...
                           'Cross-Sections/Hayashi_CF4.txt', ...
                           'Cross-Sections/Hayashi_CO2.txt', ...
                           'Cross-Sections/Hayashi_N2O.txt', ...
                           'Cross-Sections/Hayashi_SO2.txt', ...                           
                           'Cross-Sections/Hayashi_Si2H6.txt']; 
                       
Biagi897_Ref2Test_string = ['Cross-Sections/Biagi_Ar.txt',  ...
                            'Cross-Sections/Biagi_H2.txt', ...
                            'Cross-Sections/Biagi_He.txt', ...
                            'Cross-Sections/Biagi_Kr.txt', ...
                            'Cross-Sections/Biagi_N2.txt', ...
                            'Cross-Sections/Biagi_O2.txt', ...
                            'Cross-Sections/Biagi_Xe.txt'];
                        
Biagi890_Ref2Test_string = ['Cross-Sections/Biagi_Ne.txt'];   
Biagi106_Ref2Test_string = ['Cross-Sections/Biagi_SF6.txt']; 

HayashiCaptelli_Ref2_string = ['Cross-Sections/Hayashi_C2H2.txt',  ...
                               'Cross-Sections/Hayashi_C2H4.txt', ...
                               'Cross-Sections/Hayashi_CO2.txt']; 
                       
HayashiPitchford_Ref2Test_string = ['Cross-Sections/Hayashi_C2H6.txt', ...
                                    'Cross-Sections/Hayashi_CCl2F2.txt', ...
                                    'Cross-Sections/Hayashi_CCl4.txt', ...
                                    'Cross-Sections/Hayashi_CF4.txt', ...
                                    'Cross-Sections/Hayashi_SO2.txt', ...                           
                                    'Cross-Sections/Hayashi_Si2H6.txt'];                        

HayashiJILA_Ref2Test_string = ['Cross-Sections/Hayashi_N2O.txt'];
                                
C = textscan(fid, '%s %s %s %s %s %s %s %s %s %s %s %s' );
x = strfind(C{1,1}, 'ATTACHMENT');
N_atts(Ng) = sum(cell2mat(x));
x = strfind(C{1,1}, 'ELASTIC');
N_ela(Ng) = sum(cell2mat(x));
x = strfind(C{1,1}, 'EXCITATION');
N_excs(Ng) = sum(cell2mat(x));
x = strfind(C{1,1}, 'IONIZATION');
N_izs(Ng) = sum(cell2mat(x));

N_Xsecs(Ng) = N_atts(Ng)+N_ela(Ng)+N_excs(Ng)+N_izs(Ng);
if Xsec_disp_flag == 0
    if Ng == N_gases
        Xsec_disp_flag = 1;
    end
    disp_str = ['Gas #',num2str(Ng),': N_Xsecs=',num2str(N_Xsecs(Ng)),' | N_att=',num2str(N_atts(Ng)),' | N_exc=',num2str(N_excs(Ng)),' | N_iz=',num2str(N_izs(Ng))];
    disp(disp_str)
end

if N_atts(Ng) ~= 0
    for i_att = 1:N_atts(Ng)
        x  = strfind(C{1,1}, 'ATTACHMENT');
        xx = strfind(C{1,1}, '---');
        K = sum(cell2mat(x));
        if i_att == 1
            i = 1;
        else
            i = next;
        end
        while  isempty(x{i})
            i = i+1;
        end
        ind = i;
        ind_att = ind + 2;
        i = ind;
        while isempty(xx{i})
            i = i+1;
        end
        first = i+1;
        i = first;
        while isempty(xx{i})
            i = i+1;
        end
        last = i-1;
        N = length(first:last);
        u_thresh(i_att,Ng) = 0;
        next = last + 1 ;
        for i = 1 : N
            ind = first+i-1;
            st(i,1,i_att) = str2num(cell2mat(C{1,1}(ind)));
            st(i,2,i_att) = str2num(cell2mat(C{1,2}(ind)));
        end
        next = last + 1;
    end
end
%*************************************************************************%
%*************************************************************************%
%*************************************************************************%
%*************************************************************************%
if N_ela(Ng) ~= 0
    x = strfind(C{1,1},'ELASTIC');
    K = sum(cell2mat(x));
    i = 1;
    while  isempty(x{i})
        i = i+1;
    end
    ind = i;
    ind_ion = ind + 2;
    x = strfind(C{1,1}, '---');
    i = ind;
    while isempty(x{i})
        i = i+1;
    end
    first = i+1;
    i = first;
    while isempty(x{i})
        i = i+1;
    end
    last = i-1;
    N = length(first:last);
    u_thresh(N_atts(Ng)+N_ela(Ng),Ng) = 0;
    for i = 1 : N
        ind = first+i-1;
        st(i,1,N_atts(Ng)+N_ela(Ng)) = str2num(cell2mat(C{1,1}(ind)));
        st(i,2,N_atts(Ng)+N_ela(Ng)) = str2num(cell2mat(C{1,2}(ind)));
    end
    next = last + 3;
end
%*************************************************************************%
%*************************************************************************%
if N_excs(Ng) ~= 0
    for i_exc = 1:N_excs(Ng)
        x  = strfind(C{1,1}, 'EXCITATION');
        xx = strfind(C{1,1}, '---');
        K = sum(cell2mat(x));
        if i_exc == 1
            i = 1;
        else
            i = next;
        end
        while  isempty(x{i})
            i = i+1;
        end
        ind = i;
        ind_exc = ind + 2;
        i = ind;
        while isempty(xx{i})
            i = i+1;
        end
        first = i+1;
        i = first;
        while isempty(xx{i})
            i = i+1;
        end
        last = i-1;
        N = length(first:last);
        u_thresh(i_exc+N_atts(Ng)+N_ela(Ng),Ng) = str2num(cell2mat(C{1,1}(ind_exc)));
        next = last + 1 ;
        for i = 1 : N
            ind = first+i-1;
            st(i,1,i_exc+N_atts(Ng)+N_ela(Ng)) = str2num(cell2mat(C{1,1}(ind)));
            st(i,2,i_exc+N_atts(Ng)+N_ela(Ng)) = str2num(cell2mat(C{1,2}(ind)));
        end
        next = last + 1;
    end
end
%*************************************************************************%
%*************************************************************************%
if N_izs(Ng) ~= 0
    x  = strfind(C{1,1}, 'IONIZATION');
    xx = strfind(C{1,1}, '---');
    K = sum(cell2mat(x));
    for i_iz = 1:N_izs(Ng)
        if i_iz == 1
            i = 1;
        else
            i = next;
        end
        while  isempty(x{i})
            i = i+1;
        end
        ind = i;
        ind_iz = ind + 2;
        i = ind;
        while isempty(xx{i})
            i = i+1;
        end
        first = i+1;
        i = first;
        while isempty(xx{i})
            i = i+1;
        end
        last = i-1;
        N = length(first:last);
        u_thresh(i_iz+N_atts(Ng)+N_ela(Ng)+N_excs(Ng),Ng) = str2num(cell2mat(C{1,1}(ind_iz)));
        next = last + 1 ;
        for i = 1 : N
            ind = first+i-1;
            st(i,1,i_iz+N_atts(Ng)+N_ela(Ng)+N_excs(Ng)) = str2num(cell2mat(C{1,1}(ind)));
            st(i,2,i_iz+N_atts(Ng)+N_ela(Ng)+N_excs(Ng)) = str2num(cell2mat(C{1,2}(ind)));
        end
        next = last;
    end
end
%*************************************************************************%
%*************************************************************************%

fclose(fid);

% s =@ (ug,nxg) interp1(st(st(:,1,nxg)~=0,1,nxg),st(st(:,1,nxg)~=0,2,nxg),ug,'linear','extrap');
s =@ (ug,nxg) interp1q(st(st(:,1,nxg)~=0,1,nxg),st(st(:,1,nxg)~=0,2,nxg),ug);

% Linear interpolation and Born-like extrapolation of the cross-sections to
% the numerical grid.
for k1 = uv
    if N_atts(Ng) ~= 0
        for i_att = 1:N_atts(Ng)
            if  (ue(k1)/qe<=max(st(:,1,i_att)))
                 s_atte(k1,i_att,Ng) = s(ue(k1)/qe,i_att);
            else
                uma = max(st(:,1,i_att));
                ug = ue(k1)/qe;
                NF = (log(ug)/ug)/(log(uma)/uma);
                s_atte(k1,i_att,Ng) = s(uma,i_att)*NF;
            end
        end    
        for i_att = 1:N_atts(Ng)
            if  (uo(k1)/qe<=max(st(:,1,i_att)))
                 s_atto(k1,i_att,Ng) = s(uo(k1)/qe,i_att);
            else
                uma = max(st(:,1,i_att));
                ug = uo(k1)/qe;
                NF = (log(ug)/ug)/(log(uma)/uma);
                s_atto(k1,i_att,Ng) = s(uma,i_att)*NF;
            end
        end    
    else
        s_atte(k1,1,Ng) = 0;
        s_atto(k1,1,Ng) = 0;
    end
    
    if ue(k1)/qe <= max(st(:,1,N_ela(Ng)+N_atts(Ng)))
        s_me(k1,Ng) = s(ue(k1)/qe,N_ela(Ng)+N_atts(Ng));
    else
        uma = max(st(:,1,N_ela(Ng)+N_atts(Ng)));
        ug = ue(k1)/qe;
        NF = (log(ug)/ug)/(log(uma)/uma);
        s_me(k1,Ng) = s(uma,N_ela(Ng)+N_atts(Ng))*NF;
    end
    if uo(k1)/qe <= max(st(:,1,N_ela(Ng)+N_atts(Ng)))
        s_mo(k1,Ng) = s(uo(k1)/qe,N_ela(Ng)+N_atts(Ng));
    else
        uma = max(st(:,1,N_ela(Ng)+N_atts(Ng)));
        ug = uo(k1)/qe;
        NF = (log(ug)/ug)/(log(uma)/uma);
        s_mo(k1,Ng) = s(uma,N_ela(Ng)+N_atts(Ng))*NF;
    end
    
    for i_exc = 1:N_excs(Ng)
        if ue(k1)<u_thresh(i_exc+N_ela(Ng)+N_atts(Ng),Ng)*qe
            s_exce(k1,i_exc,Ng) = 0;
        elseif (ue(k1)/qe>=u_thresh(i_exc+N_ela(Ng)+N_atts(Ng),Ng)*qe) && (ue(k1)/qe<=max(st(:,1,i_exc+N_ela(Ng)+N_atts(Ng))))
             s_exce(k1,i_exc,Ng) = s(ue(k1)/qe,i_exc+N_ela(Ng)+N_atts(Ng));
        else
            uma = max(st(:,1,i_exc+N_ela(Ng)+N_atts(Ng)));
            ug = ue(k1)/qe;
            NF = (log(ug)/ug)/(log(uma)/uma);
            s_exce(k1,i_exc,Ng) = s(uma,i_exc+N_ela(Ng)+N_atts(Ng))*NF;
        end
    end    
    for i_exc = 1:N_excs(Ng)
        if uo(k1)<u_thresh(i_exc+N_ela(Ng)+N_atts(Ng),Ng)*qe
            s_exco(k1,i_exc,Ng) = 0;
        elseif (uo(k1)/qe>=u_thresh(i_exc+N_ela(Ng)+N_atts(Ng),Ng)*qe) && (uo(k1)/qe<=max(st(:,1,i_exc+N_ela(Ng)+N_atts(Ng))))
             s_exco(k1,i_exc,Ng) = s(uo(k1)/qe,i_exc+N_ela(Ng)+N_atts(Ng));
        else
            uma = max(st(:,1,i_exc+N_ela(Ng)+N_atts(Ng)));
            ug = uo(k1)/qe;
            NF = (log(ug)/ug)/(log(uma)/uma);
            s_exco(k1,i_exc,Ng) = s(uma,i_exc+N_ela(Ng)+N_atts(Ng))*NF;
        end
    end    
    
    for i_iz = 1:N_izs(Ng)
        if ue(k1)<u_thresh(i_iz+N_ela(Ng)+N_atts(Ng)+N_excs(Ng),Ng)*qe
            s_ize(k1,i_iz,Ng) = 0;
        elseif (ue(k1)/qe>=u_thresh(i_iz+N_ela(Ng)+N_atts(Ng)+N_excs(Ng))*qe) && (ue(k1)/qe<=max(st(:,1,i_iz+N_ela(Ng)+N_atts(Ng)+N_excs(Ng))))
             s_ize(k1,i_iz,Ng) = s(ue(k1)/qe,i_iz+N_ela(Ng)+N_atts(Ng)+N_excs(Ng));
        else
            uma = max(st(:,1,i_iz+N_ela(Ng)+N_atts(Ng)+N_excs(Ng)));
            ug = ue(k1)/qe;
            NF = (log(ug)/ug)/(log(uma)/uma);
            s_ize(k1,i_iz,Ng) = s(uma,i_iz+N_ela(Ng)+N_atts(Ng)+N_excs(Ng))*NF;
        end
    end    
    for i_iz = 1:N_izs(Ng)
        if uo(k1)<u_thresh(i_iz+N_ela(Ng)+N_atts(Ng)+N_excs(Ng),Ng)*qe
            s_izo(k1,i_iz,Ng) = 0;
        elseif (uo(k1)/qe>=u_thresh(i_iz+N_ela(Ng)+N_atts(Ng)+N_excs(Ng))*qe) && (uo(k1)/qe<=max(st(:,1,i_iz+N_ela(Ng)+N_atts(Ng)+N_excs(Ng))))
             s_izo(k1,i_iz,Ng) = s(uo(k1)/qe,i_iz+N_ela(Ng)+N_atts(Ng)+N_excs(Ng));
        else
            uma = max(st(:,1,i_iz+N_ela(Ng)+N_atts(Ng)+N_excs(Ng)));
            ug = uo(k1)/qe;
            NF = (log(ug)/ug)/(log(uma)/uma);
            s_izo(k1,i_iz,Ng) = s(uma,i_iz+N_ela(Ng)+N_atts(Ng)+N_excs(Ng))*NF;
        end
    end  
end

s_atte(s_atte<0) = 0;
s_atto(s_atto<0) = 0;
s_exce(s_exce<0) = 0;
s_exco(s_exco<0) = 0;
s_ize(s_ize<0) = 0;
s_izo(s_izo<0) = 0;

%---- Near the threshold energy, or at the upper limit, interp1q can sometimes return 'NaN'. 
s_atte(isnan(s_atte)==1) = 0;
s_atto(isnan(s_atto)==1) = 0;
s_exce(isnan(s_exce)==1) = 0;
s_exco(isnan(s_exco)==1) = 0;
s_ize(isnan(s_ize)==1) = 0;
s_izo(isnan(s_izo)==1) = 0;

s_Te(uv,Ng) = s_me(uv,Ng);
s_To(uv,Ng) = s_mo(uv,Ng);
for i_att = 1:N_atts(Ng)
    s_Te(uv,Ng) = s_Te(uv,Ng) + squeeze(s_atte(uv,i_att,Ng));
    s_To(uv,Ng) = s_To(uv,Ng) + squeeze(s_atto(uv,i_att,Ng));        
end
for i_exc = 1:N_excs(Ng)
    s_Te(uv,Ng) = s_Te(uv,Ng) + squeeze(s_exce(uv,i_exc,Ng));
    s_To(uv,Ng) = s_To(uv,Ng) + squeeze(s_exco(uv,i_exc,Ng));        
end
for i_iz = 1:N_izs(Ng)
    s_Te(uv,Ng) = s_Te(uv,Ng) + squeeze(s_ize(uv,i_iz,Ng));
    s_To(uv,Ng) = s_To(uv,Ng) + squeeze(s_izo(uv,i_iz,Ng));        
end

s_Te_gas(uv,Ng) = frac(Ng)*s_Te(Ng);

for i_exc = 1:N_excs(Ng)
    dk_exc(i_exc,Ng) = round(u_thresh(N_atts(Ng)+N_ela(Ng)+i_exc,Ng)*qe/Du); % Number of energy steps for excitation
end
for i_iz = 1:N_izs(Ng)
    dk_ize(i_iz,Ng) = round(u_thresh(N_atts(Ng)+N_ela(Ng)+N_excs(Ng)+i_iz,Ng)*qe/Du);               % Number of energy steps for ionization 
end
