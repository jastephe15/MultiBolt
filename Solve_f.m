% The system is organized into a typical linear system of eqs. as
% A*x=b, so that x = b\A. In this case, the desired distribution
% functions are in the 'x' vector  

A_f(1:Nu*N_terms,1:Nu*N_terms) = 0; % Reset the coefficient matrix to zero
for l1 = 0:N_terms-1
    for k1 = 1:Nu       % row #
        for k2 = 1:Nu   % column #
            if l1 == 0  % Use the l='even' eqs.
                if k1 == k2 
                    A_f(k1,k2) = +HD*Np*k_iz_eff*(ue(k1).^0.5)/((2/me)^0.5); % f_0(k)
                    A_f(k1,k2+Nu) = qe*E0*(l1+1)/(2*l1+3)*ue(k1)/Du ...
                                   +qe*E0*(l1+1)/(2*l1+3)*(l1+2)/2/2 ...
                                   +(l1+1)/(2*l1+3)*ue(k1)/2*alpha_N*Np*SST;     % f_1(k)  
                elseif k1 == k2 + 1
                    A_f(k1,k2+Nu) = -qe*E0*(l1+1)/(2*l1+3)*ue(k1)/Du ...
                                    +qe*E0*(l1+1)/(2*l1+3)*(l1+2)/2/2 ...
                                    +(l1+1)/(2*l1+3)*ue(k1)/2*alpha_N*Np*SST;  % f_1(k-1) 
                end
                for Ng = 1:N_gases
                    if k1 == k2 
                        A_f(k1,k2) = A_f(k1,k2) + frac(Ng)*2*me/mN(Ng)/Du*ue(k1)^2*Np*s_me(k1,Ng) ...
                                                + frac(Ng)*ue(k1)*Np*(s_Te(k1,Ng)-s_me(k1,Ng));
                    elseif k1 == k2 - 1
                        A_f(k1,k2) = A_f(k1,k2) - frac(Ng)*2*me/mN(Ng)/Du*ue(k2)^2*Np*s_me(k2,Ng);     % f_0(k+1)

                    end
                    for i_exc = 1:N_excs(Ng)
                        if (k2 == k1 + dk_exc(i_exc,Ng))
                            A_f(k1,k2) = A_f(k1,k2) - frac(Ng)*ue(k2)*Np*s_exce(k2,i_exc,Ng);       % f_0(k+dk_exc)  
                        end
                    end
                    for i_iz = 1:N_izs(Ng)
                        if (k2==2*k1+dk_ize(i_iz,Ng) || k2==2*k1+dk_ize(i_iz,Ng)-1)
                            A_f(k1,k2) = A_f(k1,k2) - frac(Ng)*2*ue(k2)*Np*s_ize(k2,i_iz,Ng);                  % f_0(k+dk_iz_eff)
                        end
                    end
                end
            elseif 1 <= l1 && l1 < N_terms-1
                if mod(l1,2) == 0 % l1='even'
                    if k1 == k2
                        for Ng = 1:N_gases
                            A_f(k1+l1*Nu,k2+l1*Nu) = A_f(k1+l1*Nu,k2+l1*Nu) ...
                                                     +frac(Ng)*ue(k1)*Np*s_Te(k1,Ng);   % f_l(k)
                        end
                        A_f(k1+l1*Nu,k2+l1*Nu) =  A_f(k1+l1*Nu,k2+l1*Nu) + HD*Np*k_iz_eff*(ue(k1).^0.5)/((2/me)^0.5);   % f_l(k)
                        A_f(k1+l1*Nu,k2+Nu+l1*Nu) = +qe*E0*(l1+1)/(2*l1+3)*ue(k1)/Du ...
                                                    +qe*E0*(l1+1)/(2*l1+3)*(l1+2)/2/2 ...
                                                    +(l1+1)/(2*l1+3)*ue(k1)/2*alpha_N*Np*SST;     % f_(l+1)(k)  
                        A_f(k1+l1*Nu,k2-Nu+l1*Nu) = +qe*E0*l1/(2*l1-1)*ue(k1)/Du ...
                                                    -qe*E0*l1/(2*l1-1)*(l1-1)/2/2 ...
                                                    +l1/(2*l1-1)*ue(k1)/2*alpha_N*Np*SST;  % f_(l-1)(k)
                    elseif k1 == k2 + 1
                        A_f(k1+l1*Nu,k2+Nu+l1*Nu) = -qe*E0*(l1+1)/(2*l1+3)*ue(k1)/Du ...
                                                    +qe*E0*(l1+1)/(2*l1+3)*(l1+2)/2/2 ...
                                                    +(l1+1)/(2*l1+3)*ue(k1)/2*alpha_N*Np*SST; % f_(l+1)(k-1)
                        A_f(k1+l1*Nu,k2-Nu+l1*Nu) = -qe*E0*l1/(2*l1-1)*ue(k1)/Du ...
                                                    -qe*E0*l1/(2*l1-1)*(l1-1)/2/2 ...
                                                    +l1/(2*l1-1)*ue(k1)/2*alpha_N*Np*SST;    % f_(l-1)(k-1)                                
                    end
                else % l1 ='odd'
                    if k1 == k2 
                        for Ng = 1:N_gases
                            A_f(k1+l1*Nu,k2+l1*Nu) = A_f(k1+l1*Nu,k2+l1*Nu) ...
                                                     + frac(Ng)*uo(k1)*Np*s_To(k1,Ng);   % f_l(k)
                        end
                        A_f(k1+l1*Nu,k2+l1*Nu) =  A_f(k1+l1*Nu,k2+l1*Nu) + HD*Np*k_iz_eff*(uo(k1).^0.5)/((2/me)^0.5);   % f_l(k)                 
                        A_f(k1+l1*Nu,k2-Nu+l1*Nu) =  -qe*E0*l1/(2*l1-1)*uo(k1)/Du ...
                                                     -qe*E0*l1/(2*l1-1)*(l1-1)/2/2 ...
                                                     +l1/(2*l1-1)*uo(k1)/2*alpha_N*Np*SST;      % f_(l-1)(k)
                        A_f(k1+l1*Nu,k2+Nu+l1*Nu) =  -qe*E0*(l1+1)/(2*l1+3)*uo(k1)/Du ...
                                                     +qe*E0*(l1+1)/(2*l1+3)*(l1+2)/2/2 ...
                                                     +(l1+1)/(2*l1+3)*uo(k1)/2*alpha_N*Np*SST;  % f_(l+1)(k)
                    elseif k1 == k2 - 1
                        A_f(k1+l1*Nu,k2-Nu+l1*Nu) = +qe*E0*l1/(2*l1-1)*uo(k1)/Du ...
                                                    -qe*E0*l1/(2*l1-1)*(l1-1)/2/2 ...
                                                    +l1/(2*l1-1)*uo(k1)/2*alpha_N*Np*SST;       % f_(l-1)(k+1)
                        A_f(k1+l1*Nu,k2+Nu+l1*Nu) = +qe*E0*(l1+1)/(2*l1+3)*uo(k1)/Du ...
                                                    +qe*E0*(l1+1)/(2*l1+3)*(l1+2)/2/2 ...
                                                    +(l1+1)/(2*l1+3)*uo(k1)/2*alpha_N*Np*SST;   % f_(l+1)(k+1)
                    end
                end
            elseif  l1 == N_terms-1 % l1='odd'
                if k1 == k2 
                    for Ng = 1:N_gases
                        A_f(k1+l1*Nu,k2+l1*Nu) = A_f(k1+l1*Nu,k2+l1*Nu) ...
                                                 + frac(Ng)*uo(k1)*Np*s_To(k1,Ng);   % f_l(k)
                    end
                    A_f(k1+l1*Nu,k2+l1*Nu) =  A_f(k1+l1*Nu,k2+l1*Nu) + HD*Np*k_iz_eff*(uo(k1).^0.5)/((2/me)^0.5);   % f_l(k)
                    A_f(k1+l1*Nu,k2-Nu+l1*Nu) =  -qe*E0*l1/(2*l1-1)*uo(k1)/Du ...
                                                 -qe*E0*l1/(2*l1-1)*(l1-1)/2/2 ...
                                                 +l1/(2*l1-1)*uo(k1)/2*alpha_N*Np*SST;  % f_(l-1)(k)
                elseif k1 == k2 - 1
                    A_f(k1+l1*Nu,k2-Nu+l1*Nu) = qe*E0*l1/(2*l1-1)*uo(k1)/Du ...
                                               -qe*E0*l1/(2*l1-1)*(l1-1)/2/2 ...
                                               +l1/(2*l1-1)*uo(k1)/2*alpha_N*Np*SST;     % f_(l-1)(k+1)
                end
            end
        end
    end
end
A_f_temp = A_f;
% Scale the coefficient matrix to avoid machine precision problems
A_f = A_f/max(max(A_f)); 
b_f(1:Nu*N_terms) = 0;

% Normalization condition (i.e. int(f*sqrt(u),u)=1)
A_f(1,1:Nu*N_terms) = 0;   
A_f(1,uv) = (((ue/qe).^0.5)*Du/qe);
b_f(1) = 1;

% Boundary conditions
% (odd BC is subtly incorporated in the discretization scheme and does not require an explicit fixed point)
for l1 = 1:N_terms-1
    if mod(l1,2) == 0 % l1 = (even), setup even BC 
        % f^l(u=u_max) = 0; for l=(even)
        % A_f(1+l1*Nu,l1*Nu+uv) = 0;
        A_f(1+l1*Nu,1:end) = 0;
        A_f(1+l1*Nu,l1*Nu+1) = 1;
    end
end  
A_f = sparse(A_f);
x_f = (A_f\(b_f'));     % Solve the A*x=b system
f_0 = x_f(1:Nu)';    % The isotropic portion of the solution (i.e. the EEDF)
f_1 = x_f(Nu+1:2*Nu)'; % The first anisotropy