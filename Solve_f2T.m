omega2 = (2*DFT_N+DFL_N)/sqrt(3)/Np; % Initial guess, not actually omega2
for iter3 = 1:iter_max
    if iter3 == 1
        A_f2T(1:Nu*N_terms,1:Nu*N_terms) = 0;
        for l1 = 0:N_terms-1
            for k1 = 1:Nu       % row #
                for k2 = 1:Nu   % column #
                    if l1 == 0  % Use the l='even' eqs.
                        if k1 == k2 
                            A_f2T(k1,k2) = +HD*Np*k_iz_eff*(ue(k1).^0.5)/((2/me)^0.5); % f_0(k)
                            A_f2T(k1,k2+Nu) = qe*E0*(l1+1)/(2*l1+3)*ue(k1)/Du ...
                                             +qe*E0*(l1+1)/(2*l1+3)*(l1+2)/2/2 ...
                                             +(l1+1)/(2*l1+3)*ue(k1)/2*alpha_N*Np*SST;     % f_1(k)  
                        elseif k1 == k2 + 1
                            A_f2T(k1,k2+Nu) = -qe*E0*(l1+1)/(2*l1+3)*ue(k1)/Du ...
                                              +qe*E0*(l1+1)/(2*l1+3)*(l1+2)/2/2 ...
                                              +(l1+1)/(2*l1+3)*ue(k1)/2*alpha_N*Np*SST;  % f_1(k-1) 
                        end
                        for Ng = 1:N_gases
                            if k1 == k2 
                                A_f2T(k1,k2) = A_f2T(k1,k2) + frac(Ng)*2*me/mN(Ng)/Du*ue(k1)^2*Np*s_me(k1,Ng) ...
                                                            + frac(Ng)*ue(k1)*Np*(s_Te(k1,Ng)-s_me(k1,Ng));
                            elseif k1 == k2 - 1
                                A_f2T(k1,k2) = A_f2T(k1,k2) - frac(Ng)*2*me/mN(Ng)/Du*ue(k2)^2*Np*s_me(k2,Ng);     % f_0(k+1)
                            end
                            for i_exc = 1:N_excs(Ng)
                                if (k2 == k1 + dk_exc(i_exc,Ng))
                                    A_f2T(k1,k2) = A_f2T(k1,k2) - frac(Ng)*ue(k2)*Np*s_exce(k2,i_exc,Ng);       % f_0(k+dk_exc)  
                                end
                            end
                            for i_iz = 1:N_izs(Ng)
                                if (k2==2*k1+dk_ize(i_iz,Ng) || k2==2*k1+dk_ize(i_iz,Ng)-1)
                                    A_f2T(k1,k2) = A_f2T(k1,k2) - frac(Ng)*2*ue(k2)*Np*s_ize(k2,i_iz,Ng);                  % f_0(k+dk_iz_eff)
                                end
                            end
                        end
                    elseif 1 <= l1 && l1 < N_terms-1
                        if mod(l1,2) == 0 % l1='even'
                            if k1 == k2
                                for Ng = 1:N_gases
                                    A_f2T(k1+l1*Nu,k2+l1*Nu) = A_f2T(k1+l1*Nu,k2+l1*Nu) ...
                                                              +frac(Ng)*ue(k1)*Np*s_Te(k1,Ng);   % f_l(k)
                                end
                                A_f2T(k1+l1*Nu,k2+l1*Nu) =  A_f2T(k1+l1*Nu,k2+l1*Nu) + HD*Np*k_iz_eff*(ue(k1).^0.5)/((2/me)^0.5);   % f_l(k)
                                A_f2T(k1+l1*Nu,k2+Nu+l1*Nu) = +qe*E0*(l1+1)/(2*l1+3)*ue(k1)/Du ...
                                                              +qe*E0*(l1+1)/(2*l1+3)*(l1+2)/2/2;     % f_(l+1)(k)  
                                A_f2T(k1+l1*Nu,k2-Nu+l1*Nu) = +qe*E0*l1/(2*l1-1)*ue(k1)/Du ...
                                                              -qe*E0*l1/(2*l1-1)*(l1-1)/2/2;  % f_(l-1)(k)
                            elseif k1 == k2 + 1
                                A_f2T(k1+l1*Nu,k2+Nu+l1*Nu) = -qe*E0*(l1+1)/(2*l1+3)*ue(k1)/Du ...
                                                              +qe*E0*(l1+1)/(2*l1+3)*(l1+2)/2/2; % f_(l+1)(k-1)
                                A_f2T(k1+l1*Nu,k2-Nu+l1*Nu) = -qe*E0*l1/(2*l1-1)*ue(k1)/Du ...
                                                              -qe*E0*l1/(2*l1-1)*(l1-1)/2/2;    % f_(l-1)(k-1)                                
                            end
                        else % l1 ='odd'
                            if k1 == k2 
                                for Ng = 1:N_gases
                                    A_f2T(k1+l1*Nu,k2+l1*Nu) = A_f2T(k1+l1*Nu,k2+l1*Nu) ...
                                                              +frac(Ng)*uo(k1)*Np*s_To(k1,Ng);   % f_l(k)
                                end
                                A_f2T(k1+l1*Nu,k2+l1*Nu) =  A_f2T(k1+l1*Nu,k2+l1*Nu) + HD*Np*k_iz_eff*(uo(k1).^0.5)/((2/me)^0.5);   % f_l(k)
                                A_f2T(k1+l1*Nu,k2-Nu+l1*Nu) =  -qe*E0*l1/(2*l1-1)*uo(k1)/Du ...
                                                               -qe*E0*l1/(2*l1-1)*(l1-1)/2/2;      % f_(l-1)(k)
                                A_f2T(k1+l1*Nu,k2+Nu+l1*Nu) =  -qe*E0*(l1+1)/(2*l1+3)*uo(k1)/Du ...
                                                               +qe*E0*(l1+1)/(2*l1+3)*(l1+2)/2/2;  % f_(l+1)(k)
                            elseif k1 == k2 - 1
                                A_f2T(k1+l1*Nu,k2-Nu+l1*Nu) = +qe*E0*l1/(2*l1-1)*uo(k1)/Du ...
                                                              -qe*E0*l1/(2*l1-1)*(l1-1)/2/2;       % f_(l-1)(k+1)
                                A_f2T(k1+l1*Nu,k2+Nu+l1*Nu) = +qe*E0*(l1+1)/(2*l1+3)*uo(k1)/Du ...
                                                              +qe*E0*(l1+1)/(2*l1+3)*(l1+2)/2/2;   % f_(l+1)(k+1)
                            end
                        end
                    elseif  l1 == N_terms-1 % l1='odd'
                        if k1 == k2
                            for Ng = 1:N_gases
                                A_f2T(k1+l1*Nu,k2+l1*Nu) = A_f2T(k1+l1*Nu,k2+l1*Nu) ...
                                                          +frac(Ng)*uo(k1)*Np*s_To(k1,Ng);   % f_l(k)
                            end
                            A_f2T(k1+l1*Nu,k2+l1*Nu) =  A_f2T(k1+l1*Nu,k2+l1*Nu) + HD*Np*k_iz_eff*(uo(k1).^0.5)/((2/me)^0.5);   % f_l(k)
                            A_f2T(k1+l1*Nu,k2-Nu+l1*Nu) = -qe*E0*l1/(2*l1-1)*uo(k1)/Du ...
                                                          -qe*E0*l1/(2*l1-1)*(l1-1)/2/2;  % f_(l-1)(k)
                        elseif k1 == k2 - 1
                            A_f2T(k1+l1*Nu,k2-Nu+l1*Nu) = qe*E0*l1/(2*l1-1)*uo(k1)/Du ...
                                                         -qe*E0*l1/(2*l1-1)*(l1-1)/2/2;     % f_(l-1)(k+1)
                        end
                    end
                end
            end
        end
    end
    b_f2T(1:Nu*N_terms) = 0;
    for l1 = 0:N_terms-1
       for k1 = 1:Nu 
           if l1 == 0
               if k1 ~= 1
                   b_f2T(k1+l1*Nu) = -((ue(k1)*me/2)^0.5)*omega2*x_f(k1+l1*Nu)*Np ...
                                     -((ue(k1)*me/2)^0.5)/sqrt(3)*omega1*x_f1L(k1+l1*Nu) ...
                                     + (l1+1)/(2*l1+3)/sqrt(3)*ue(k1)*(x_f1L(k1+l1*Nu+Nu)+x_f1L(k1+l1*Nu+Nu-1))/2 ...
                                     + (l1+1)/(2*l1+3)*(l1+2)/sqrt(3)*ue(k1)*(x_f1T(k1+l1*Nu)+x_f1T(k1+l1*Nu-1))/2;
               else
                   b_f2T(k1+l1*Nu) = -((ue(k1)*me/2)^0.5)*omega2*x_f(k1+l1*Nu)*Np ...
                                     -((ue(k1)*me/2)^0.5)/sqrt(3)*omega1*x_f1L(k1+l1*Nu) ...
                                     + (l1+1)/(2*l1+3)/sqrt(3)*ue(k1)*x_f1L(k1+l1*Nu+Nu)/2 ...
                                     + (l1+1)/(2*l1+3)*(l1+2)/sqrt(3)*ue(k1)*x_f1T(k1+l1*Nu)/2;
               end
           elseif l1 == 1
               if k1 ~= Nu
                   b_f2T(k1+l1*Nu) = -((uo(k1)*me/2)^0.5)*omega2*x_f(k1+l1*Nu)*Np ...
                                     -((uo(k1)*me/2)^0.5)/sqrt(3)*omega1*x_f1L(k1+l1*Nu) ...
                                     + l1/(2*l1-1)/sqrt(3)*uo(k1)*(x_f1L(k1+l1*Nu-Nu)+x_f1L(k1+l1*Nu-Nu+1))/2 ...
                                     + (l1+1)/(2*l1+3)/sqrt(3)*uo(k1)*(x_f1L(k1+l1*Nu+Nu)+x_f1L(k1+l1*Nu+Nu+1))/2 ...
                                     + (l1+1)/(2*l1+3)*(l1+2)/sqrt(3)*uo(k1)*(x_f1T(k1+l1*Nu)+x_f1T(k1+l1*Nu+1))/2;
               else
                   b_f2T(k1+l1*Nu) = -((uo(k1)*me/2)^0.5)*omega2*x_f(k1+l1*Nu)*Np ...
                                     -((uo(k1)*me/2)^0.5)/sqrt(3)*omega1*x_f1L(k1+l1*Nu) ...
                                     + l1/(2*l1-1)/sqrt(3)*uo(k1)*x_f1L(k1+l1*Nu-Nu)/2 ...
                                     + (l1+1)/(2*l1+3)/sqrt(3)*uo(k1)*x_f1L(k1+l1*Nu+Nu)/2 ...
                                     + (l1+1)/(2*l1+3)*(l1+2)/sqrt(3)*uo(k1)*x_f1T(k1+l1*Nu)/2;
               end             
           elseif 2 <= l1 && l1 <= N_terms-2
               if mod(l1,2) == 0 % l1='even'
                   if k1 ~= 1
                       b_f2T(k1+l1*Nu) = -((ue(k1)*me/2)^0.5)*omega2*x_f(k1+l1*Nu)*Np ... 
                                         -((ue(k1)*me/2)^0.5)/sqrt(3)*omega1*x_f1L(k1+l1*Nu) ... 
                                         + l1/(2*l1-1)/sqrt(3)*ue(k1)*(x_f1L(k1+l1*Nu-Nu)+x_f1L(k1+l1*Nu-Nu-1))/2 ... 
                                         + (l1+1)/(2*l1+3)/sqrt(3)*ue(k1)*(x_f1L(k1+l1*Nu+Nu)+x_f1L(k1+l1*Nu+Nu-1))/2 ... 
                                         - l1/(2*l1-1)*(l1-1)/sqrt(3)*ue(k1)*(x_f1T(k1+l1*Nu-2*Nu)+x_f1T(k1+l1*Nu-2*Nu-1))/2 ... 
                                         + (l1+1)/(2*l1+3)*(l1+2)/sqrt(3)*ue(k1)*(x_f1T(k1+l1*Nu)+x_f1T(k1+l1*Nu-1))/2; 
                   else
                       b_f2T(k1+l1*Nu) = -((ue(k1)*me/2)^0.5)*omega2*x_f(k1+l1*Nu)*Np ...
                                         -((ue(k1)*me/2)^0.5)/sqrt(3)*omega1*x_f1L(k1+l1*Nu) ...
                                         + l1/(2*l1-1)/sqrt(3)*ue(k1)*x_f1L(k1+l1*Nu-Nu)/2 ...
                                         + (l1+1)/(2*l1+3)/sqrt(3)*ue(k1)*x_f1L(k1+l1*Nu+Nu)/2 ...
                                         - l1/(2*l1-1)*(l1-1)/sqrt(3)*ue(k1)*x_f1T(k1+l1*Nu-2*Nu)/2 ...
                                         + (l1+1)/(2*l1+3)*(l1+2)/sqrt(3)*ue(k1)*x_f1T(k1+l1*Nu)/2;
                   end
               else % l1='odd'
                   if k1 ~= Nu
                       b_f2T(k1+l1*Nu) = -((uo(k1)*me/2)^0.5)*omega2*x_f(k1+l1*Nu)*Np ...
                                         -((uo(k1)*me/2)^0.5)/sqrt(3)*omega1*x_f1L(k1+l1*Nu) ...
                                         + l1/(2*l1-1)/sqrt(3)*uo(k1)*(x_f1L(k1+l1*Nu-Nu)+x_f1L(k1+l1*Nu-Nu+1))/2 ...
                                         + (l1+1)/(2*l1+3)/sqrt(3)*uo(k1)*(x_f1L(k1+l1*Nu+Nu)+x_f1L(k1+l1*Nu+Nu+1))/2 ...
                                         - l1/(2*l1-1)*(l1-1)/sqrt(3)*uo(k1)*(x_f1T(k1+l1*Nu-2*Nu)+x_f1T(k1+l1*Nu-2*Nu+1))/2 ...
                                         + (l1+1)/(2*l1+3)*(l1+2)/sqrt(3)*uo(k1)*(x_f1T(k1+l1*Nu)+x_f1T(k1+l1*Nu+1))/2;
                   else
                       b_f2T(k1+l1*Nu) = -((uo(k1)*me/2)^0.5)*omega2*x_f(k1+l1*Nu)*Np ...
                                         -((uo(k1)*me/2)^0.5)/sqrt(3)*omega1*x_f1L(k1+l1*Nu) ...
                                         + l1/(2*l1-1)/sqrt(3)*uo(k1)*x_f1L(k1+l1*Nu-Nu)/2 ...
                                         + (l1+1)/(2*l1+3)/sqrt(3)*uo(k1)*x_f1L(k1+l1*Nu+Nu)/2 ...
                                         - l1/(2*l1-1)*(l1-1)/sqrt(3)*uo(k1)*x_f1T(k1+l1*Nu-2*Nu)/2 ...
                                         + (l1+1)/(2*l1+3)*(l1+2)/sqrt(3)*uo(k1)*x_f1T(k1+l1*Nu)/2;
                   end
               end
           elseif l1 == N_terms-1
               if k1 ~= Nu
                   b_f2T(k1+l1*Nu) = -((uo(k1)*me/2)^0.5)*omega2*x_f(k1+l1*Nu)*Np ...
                                     -((uo(k1)*me/2)^0.5)/sqrt(3)*omega1*x_f1L(k1+l1*Nu) ...
                                     + l1/(2*l1-1)/sqrt(3)*uo(k1)*(x_f1L(k1+l1*Nu-Nu)+x_f1L(k1+l1*Nu-Nu+1))/2 ...
                                     - l1/(2*l1-1)*(l1-1)/sqrt(3)*uo(k1)*(x_f1T(k1+l1*Nu-2*Nu)+x_f1T(k1+l1*Nu-2*Nu+1))/2;
               else
                   b_f2T(k1+l1*Nu) = -((uo(k1)*me/2)^0.5)*omega2*x_f(k1+l1*Nu)*Np ...
                                     -((uo(k1)*me/2)^0.5)/sqrt(3)*omega1*x_f1L(k1+l1*Nu) ...
                                     + l1/(2*l1-1)/sqrt(3)*uo(k1)*x_f1L(k1+l1*Nu-Nu)/2 ...
                                     - l1/(2*l1-1)*(l1-1)/sqrt(3)*uo(k1)*x_f1T(k1+l1*Nu-2*Nu)/2;
               end
           end
       end
    end
    b_f2T = b_f2T*Np;

    % Scale the matrix to avoid machine precision problems
    % b_f2T = b_f2T/max(max(A_f2T));
    % A_f2T = A_f2T/max(max(A_f2T)); 


    % Normalization condition
    A_f2T(Nu,1:Nu*N_terms) = 0;   
    A_f2T(Nu,uv) = (((ue/qe).^0.5)*Du/qe);
    b_f2T(Nu) = 0;
    
    % b_f2z = b_f2z';
    A_f2T = sparse(A_f2T);
    x_f2T = (A_f2T\(b_f2T')); % Solve the system
    f2T_0(1:Nu) = x_f2T(1:Nu);
    S2T_att = 0;
    for Ng = 1:N_gases
        for i_att = 1:N_atts(Ng)
            S2T_att(i_att,Ng) = sqrt(2*qe/me)*trapz(ue(uv)/qe,squeeze(frac(Ng)*s_atte(uv,i_att,Ng))'.*f2T_0(uv).*ue(uv).^1.0/qe)/Np;
        end
        for i_iz = 1:N_izs(Ng)
            S2T_iz(i_iz,Ng) = sqrt(2*qe/me)*trapz(ue(uv)/qe,squeeze(frac(Ng)*s_ize(uv,i_iz,Ng))'.*f2T_0(uv).*ue(uv).^1.0/qe)/Np;
        end
    end
    S2T = sum(sum(S2T_iz))-sum(sum(S2T_att));
    
    omega2_NEW = (2*DFT_N+DFL_N)/sqrt(3)/Np + S2T;
    if abs(abs(abs(omega2_NEW-omega2)/omega2_NEW))<conv_err && iter3 > 5 % iterate until omega2 converges. This one can be tricky, keep an eye on it!
        omega2 = omega2_NEW;
        converged_f2T(is) = 1; % Useful flag for user feedback
        break
    elseif abs(abs(abs(omega2_NEW-omega2)/omega2_NEW))>conv_err && iter3 == iter_max
        disp('Warning: f_2T solution did not converge')
        converged_f2T(is) = 0;
    end
    omega2 = omega2_NEW;
end
